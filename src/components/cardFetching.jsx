import React, { Component } from 'react';
import Form from "./form"

class CardOfList extends Component {
  state = {
    cardsDetails: [],
    inputForCard: ' ',
    open: false
  };

  handleChange(event) {
    this.setState({ inputForCard: event.target.value });
  }
  componentDidMount() {
    const url = `https://api.trello.com/1/lists/${this.props.listDetails.id}/cards?key=0a888fcd467afb859a113e18472a165a&token=f287454275494e79765ee9355d8d4678edffe624889a85aa91fa254571b4bb14`;
    fetch(url, {
      method: 'Get'
    })
      .then(res => res.json())
      .then(json => this.setState({ cardsDetails: json }));
  }
  handleAddcard = cardName => {
    fetch(
      `https://api.trello.com/1/cards?name=${cardName}&idList=${this.props.listDetails.id}&keepFromSource=all&key=0a888fcd467afb859a113e18472a165a&token=f287454275494e79765ee9355d8d4678edffe624889a85aa91fa254571b4bb14`,
      { method: 'POST' }
    )
      .then(response => response.json())
      .then(element => {
        this.setState({
          cardsDetails: this.state.cardsDetails.concat([element])
        });
      });
  };
  handleDeleteCard = deleteCard => {
    this.setState({
      cardsDetails: this.state.cardsDetails.filter(
        card => card.id !== deleteCard
      )
    });
    fetch(
      `https://api.trello.com/1/cards/${deleteCard}?key=0a888fcd467afb859a113e18472a165a&token=f287454275494e79765ee9355d8d4678edffe624889a85aa91fa254571b4bb14`,
      { method: 'DELETE' }
    );
  };
  render() {
    //   console.log(this.state.data);
    return (
      <div className="cards">
        <div className="card-body">
          {this.state.cardsDetails.map(eachCard => (
            <div className="card">
            <button
              type="button"
              className="btn btn-card"
              onClick={() => {
                this.props.onOpenModal(eachCard);
              }}
            >
              {eachCard.name}
            </button>
            <button
              className="btn btn-close"
              onClick={() => {
                this.handleDeleteCard(eachCard.id);
              }}
            >
              X
            </button>
          </div>
          ))}
        </div>
        <Form 
        formName = "Add Card"
        handleAddcard = {this.handleAddcard} />
      </div>
    );
  }
}

export default CardOfList;
