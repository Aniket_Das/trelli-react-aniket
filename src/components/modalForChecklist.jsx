import React from 'react';
import Modal from 'react-responsive-modal';
import Checklist from './Checklist';
import Form from "./form";
// import Button from './button';
const styles = {
  fontFamily: 'sans-serif',
  textAlign: 'center'
};

class ModalComponent extends React.Component {
  // constructor() {
  //     super();
  //     console.log(this);
  //   }
  state = {
    inputForChecklist: ' ',
  };
  
  handleChange(event) {
    // console.log(this);
    this.setState({ inputForChecklist: event.target.value });
  }

  render() {
    return (
      <div style={styles}>
        <Modal open={this.props.open} onClose={this.props.closeModal} classNames = "modal">
          {this.props.checklistsDetails.map(checkList => (
                <Checklist
                  key={checkList.id}
                  checklistDetails={checkList}
                  handleAddCheckItem={this.props.handleAddCheckItem}
                  handleUpdateCheckItem={this.props.handleUpdateCheckItem}
                  handleDeleteCheckItem={this.props.handleDeleteCheckItem}
                  handleDeleteCheckList={this.props.handleDeleteCheckList}
                />
              ))}
          <Form 
            formName = "Add Checklist"
            handleAddChecklist = {this.props.handleAddChecklist}/>
        </Modal>
      </div>
    );
  }
}
export default ModalComponent;
